# 2048 Game

This project was a challenge for Miniclip, it is a 2048 game clone using Javascript.

### Game Architecture

I know that Javascript Classes are not real classes yet, but i decided to develop this game with OOP programming paradigm, my aproach on architecture was somehow creating a gameloop that as a timing (frame per second) controlling and stable the game speed on diferent computers. This gameloop calls every frame for update and draw from the StateManager which is responsible for controlling Which state is acitve and which state is needed to run update and draw functions. It as aswell eventListeners on main.js that catch every input from keyboard, mouse or touch, and tell stateManager to tell the active state to process this input. There is a diagram which explains the game architecture used in this challenge, it can be found in the folder project and it is named GameArchitecture.jpg

At the beginning i was planning these structure with a call every frame, but in the end i realised maybe there was a better solution for acomplish this. Maybe the best solution for best performance was to only call update and draw when user makes a move. It seems to make more sense to me now but, i had no time to reestruct everything or make this change.

I´m happy with the overall result, but honestly it was a bit tricky to manage this challenge and my deadlines at work at the same time. I wish I had more time to write a more elegant code, but it was not possible. But if a had, i definitely implemented a class inputManager to handle all inputs, tried to improve the sprite and vec2 and how their relation with each other, clean a litle bit more some functions and divide responsibilities in a more elegant way.

### Prerequisites

What things you need to install the software and how to install them

```
You will need:
	- node.js  (Node.js is an open source server framework). 
	- npm  (NPM is a package manager for Node.js modules).
	- Any browser of your choice.
	- Command line of your choice (i will be using cmd for windows)
```

### Installing Node.js and npm

Installing Node and NPM is pretty straightforward using the installer package available from the Node.js® (download can be made here: https://nodejs.org/en/).

Just follow software instructions and you should be reaady to go in a minute.

### Setup 

This was tested and developed in windows so i´m afraid that mac users run into problems with this steps, but i can tell one change that maybe is need to give atention if you are using a mac:

in package.json file, this line on scripts:  "open": "concurrently \"http-server -a localhost -p 1234\" \"start http://localhost:1234/dist\""
change where it says "start" to "open", It should be the unique change to be made, but again, i haven´t tested on mac, only with windows.

Open your command line tool, you should see something like this:

```
Microsoft Windows [Version 6.1.7601]
Copyright (c) 2009 Microsoft Corporation. All rights reserved.

C:\Users\yourUser>

```

Now, navigate to your project folder, in my case it was in:

```
C:\wamp64\www\2048>

```

Next we need to run build to copy assets and files optimizations to be placed in 'dist' folder through Webpack.
This can be made by typing:

```
C:\wamp64\www\2048> npm run build

```

After that, by typing the follwing ( npm run start ), It should start a local server, open the browser automatically and start the game:

```
C:\wamp64\www\2048> npm run start

```

To quit server we can always type: CTRL + C. 

### How to play

Use your mouse to navigate and arrow keys to move pieces!


### Run Unit Testing With Mocha.Js

It was my first time using Unit testing, so please go easy on me on this one!
Honestly here I had no time and my knowledge was limited of mocha dependencies to finish 100% coverage.
Anyway, i make a search on this and came up with some testing and a report!

We can run unit testing and get a clean and well presented report on our browser and project folder ( mochawesome-report ) by typing:

```
C:\wamp64\www\miniclip> npm run test

```

## Built With

* Javascript ES6
* Node.js npm

## Authors

**Nuno Barão**

## License

This project is licensed under the MIT License.

