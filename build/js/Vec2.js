/**
 * @ Vec2 file
 * @ author Nuno Barão <nunoandrebarao@outlook.pt>
 * @ version 1.0
 */

'use strict';

module.exports = class Vec2{
	
    /**
     * The Vec2 Constructor
     *
     * @param {Number} x - position x.
     * @param {Number} y - position y.
     */
	constructor(x, y){
		this.set(x, y);
	}

    /**
     * set x and y position
     *
     * @param {Number} x - position x.
     * @param {Number} y - position y.
     */
	set(x, y){
		this.x = x;
		this.y = y;
	}
}