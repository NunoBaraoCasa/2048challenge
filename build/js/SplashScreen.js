/**
 * @ SplashScreen file, animate fadein and fade out miniclip logo at the beggining
 * @ author Nuno Barão <nunoandrebarao@outlook.pt>
 * @ version 1.0
 */

'use strict';

module.exports = class SplashScreen {

	/**
     * The Splashscreen constructor
     *
     * @param {element} CANVAS - The HTML <canvas> element is used to draw graphics
     * @param {object} context - object that provides methods and properties for drawing on canvas.
     * @param {boolean} active - A flag to see if this state is active
     * @param {object} transition - Object that contains properties when, from and where to change state.
     */
	constructor(CANVAS, CONTEXT, active, transition) {
		this.canvas = CANVAS;
		this.context = CONTEXT;
		this.active = active;
		this.transition = transition;

		this.alpha = 0;
		this.is_toFadeIn = true;
		this.is_toFadeOut = false;

		this.splashImage = new Image();
		this.splashImage.src = 'images/splash_logo.png';
	}

	/**
     * Update Fade animation 
     *
     * @param {number} deltaTime - time per Frame
     */
	update(deltaTime) {
		if(this.is_toFadeIn) 
			this.fadeIn(deltaTime);

		if(this.is_toFadeOut)
			this.fadeOut(deltaTime);

		//Return transition.change to check if we need to change state
		return this.transition.change;
	}

	/**
     * fade in animation
     *
     * @param {number} deltaTime - time per Frame.
     */
	async fadeIn(deltaTime) {	
		//Set speed of fadeIn
		let speed = 50;
		//Graduali fade in image by controlling alpha
		this.alpha += deltaTime / speed;
		//If alpha greater than 1
		if(this.alpha > 1){
			//Set delay time inbetween fade in and fade out
			let delay = 1000 * deltaTime;
			//call delay function
			await this.sleep(1000);
			//Set flags to change to fade oout animation
			this.is_toFadeIn = false;
			this.is_toFadeOut = true;
		}

		return false;
	}

	/**
     * fade out animation
     *
     * @param {number} deltaTime - time per Frame.
     */
	fadeOut(deltaTime) {
		//Set speed of fadeOut
		let speed = 50;
		//Graduali fade out image by controlling alpha
		this.alpha -= deltaTime / speed;
		//If alpha less than 0
		if(this.alpha < 0) 
			this.transition.change = true;

		return false;
	}

	/**
     * draw, call clearcanvas and drawsplashscreen
     */
	draw() {
		this.clearCanvas();
		this.drawSplashScreen();
	}

	/**
     * Clear our canvas for the next frame
     */
	clearCanvas() {
		this.context.clearRect(0, 0, canvas.width, canvas.height);
	}

	/**
     * Draw splash image to canvas
     *
     */
	drawSplashScreen() {
		//Set global alpha
		this.context.globalAlpha = this.alpha;
		//Draw splash image
		this.context.drawImage(this.splashImage, (canvas.width / 2) - this.splashImage.width / 2, (canvas.height / 2) - this.splashImage.height / 2);
	}

	/**
     * Listen to keyboard input  
     *
     * @param {object} e - object that provides Event properties.
     */
	keyboardInput(e) {
		
	}

	/**
     * Listen to mouse input 
     *
     * @param {object} e - object that provides Event properties.
     */
	mouseInput(e) {

	}

	/**
     * Listen to touch input 
     * 
     * @param {object} e - object that provides Event properties
     */
    touchInput(e) {
       
    }

	/**
     * Set this state (splashscreen) active to true or false
     *
     * @param {boolean} active - A flag to know if its true or false.
     */
	setActive(active) {
		return this.active = active;
	}

	/**
     * delay inbetween fade in and fade out animation
     *
     * @param {number} miliseconds - Miliseconds to delay.
     */
	sleep(miliseconds) {
	  return new Promise(resolve => setTimeout(resolve, miliseconds));
	}
}