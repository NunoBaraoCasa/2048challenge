/**
 * @ Resources file, handle resources setup
 * @ author Nuno Barão <nunoandrebarao@outlook.pt>
 * @ version 1.0
 */

'use strict';

let Sprite = require('../js/Sprite.js');

module.exports = class Resources{
	
	/**
     * The Resources constructor
     *
     */
	constructor(){
		this.imagesSources = [];
		this.soundsSources = [];
		this.images = [];
		this.sounds = [];
		this.sprites = {};
	}

	/**
     * Push a source of an image to array imagesSources. i.e: "../images/image.png"
     *
     * @param {string} source - Source of image to push 
     */
	newImage(source) {
		return this.imagesSources.push(source);
	}

	/**
     * Push a source of a sound(audio) to array soundsSources. i.e: "../sounds/sound.mp3"
     *
     * @param {string} source - Source of sound to push 
     */
	newSound(source) {
		return this.soundsSources.push(source);
	}

	/**
     * Add a nem Sprite object to sprites object. i.e: sprites = { menuLogo: {...}, buttonPlay: {...} };
     *
     * @param {element} CANVAS - The HTML <canvas> element
     */
	addSprites(canvas) {
		this.sprites.menuLogo = new Sprite(this.images[1], 310, 255, [[0, 140]], (canvas.width/2) - 310 / 2, 50);
		this.sprites.buttonPlay = new Sprite(this.images[1], 120, 50, [[5, 442]], (canvas.width/2) - 120 / 2, 380);
		this.sprites.buttonHighScores = new Sprite(this.images[1], 260, 50, [[15, 500]], (canvas.width/2) - 260 / 2, 450);
		this.sprites.gameOver = new Sprite(this.images[1], 330, 65, [[0, 70]], (canvas.width/2) - 330 / 2, (canvas.height/2) - 65);
		this.sprites.youWin = new Sprite(this.images[1], 330, 65, [[0, 638]], (canvas.width/2) - 330 / 2, (canvas.height/2) - 65);
		this.sprites.backToMenu = new Sprite(this.images[1], 330, 65, [[0, 0]], (canvas.width/2) - 330 / 2, 500);
		
		return false;
	}
}