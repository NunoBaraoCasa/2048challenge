/**
 * @ main file, set and start the gameloop
 * @ author Nuno Barão <nunoandrebarao@outlook.pt>
 * @ version 1.0
 */

import '../css/main.scss';
import '../images/splash_logo.png';
import '../images/spritesheet.png';

let GameLoop = require('../js/GameLoop.js');

/**
 * Method to animate and make gameloop 
 */
window.requestAnimFrame = (function() {
  	return window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame ||
    function(callback) {
      window.setTimeout(callback, 1000 / 60);
    };
})();

/**
 * Canavas element and context object. 
 */
const CANVAS = document.getElementById('canvas');
const CONTEXT = CANVAS.getContext('2d');

/**
 *  Check if device is mobile
 */
let typeDevice; 
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	typeDevice = 'Smartphone';
}

if(typeDevice == 'Smartphone'){
    CANVAS.width = window.innerWidth;
    CANVAS.height = window.innerHeight;
}

/**
 * Create GameLoop object and calls run method to start Game loop. 
 */
let gameLoop = new GameLoop(CANVAS, CONTEXT, requestAnimFrame);
gameLoop.run();

/**
 * Listen to keyboard input 
 */
document.addEventListener('keydown', function(e){
	e.preventDefault();
	gameLoop.keyboardInput(e);
}, false);

/**
 * Listen to mouse input 
 */
document.addEventListener('click', function(e){
	e.preventDefault();
	gameLoop.mouseInput(e);
}, false);

/**
 * Listen to touch start input 
 */
document.addEventListener('touchstart', function(e){
	e.preventDefault();
	gameLoop.touchInput(e);
}, false);

/**
 * Listen to touch end input 
 */
document.addEventListener('touchend', function(e){
	e.preventDefault();
	gameLoop.touchInput(e);
}, false);
