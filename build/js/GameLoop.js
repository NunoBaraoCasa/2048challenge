/**
 * @ GameLoop file, run gameloop, create state manager, create states
 * @ author Nuno Barão <nunoandrebarao@outlook.pt>
 * @ version 1.0
 */

'use strict';

let Resources = require('../js/Resources.js');
let StateManager = require('../js/StateManager.js');
let SplashScreen = require('../js/SplashScreen.js');
let Loading = require('../js/Loading.js');
let Menu = require('../js/Menu.js');
let Play = require('../js/Play.js');
let HighScore = require('../js/HighScore.js');

class GameLoop{

	/**
     * The GameLoop constructor
     *
     * @param {element} CANVAS - The HTML <canvas> element is used to draw graphics
     * @param {object} CONTEXT - object that provides methods and properties for drawing on canvas.
     * @param {method} requestAnimFrame - method that calls repeatedly to create gameloop.
     */
	constructor(CANVAS, CONTEXT, requestAnimFrame){
		this.clock = setInterval(function clock() {
			let seconds = 0;
			seconds++;

			return seconds;
		}, 1000);

		this.isRunning = true;
		this.timePerFrame = 1/30;
		this.timeSinceLastUpdate = 0;

		this.resources = new Resources();
		this.stateManager = new StateManager();

		this.addResources();
		this.addStates(CANVAS, CONTEXT);

		this.requestAnimFrame = requestAnimFrame;

		this.sprites = [];
	}

	/**
     * Add sources to resources object
     */
	addResources() {
		//Images
		this.resources.newImage('images/splash_logo.png');
		this.resources.newImage('images/spritesheet.png');
		//Sounds
		//this.resources.newSound('../sounds/103.wav');
		//this.resources.newSound('../sounds/114.wav');
		return false;
	}

	/**
     * Add States to StateManager
     *
     * @param {element} CANVAS - The HTML <canvas> element is used to draw graphics
     * @param {object} CONTEXT - object that provides methods and properties for drawing on canvas.
     */
	addStates(CANVAS, CONTEXT) {
		this.stateManager.pushState('SplashScreen', new SplashScreen(CANVAS, CONTEXT, true, {change: false, from: 'SplashScreen', to: 'Loading'}));
		this.stateManager.pushState('Loading', new Loading(CANVAS, CONTEXT, this.resources, false, { change: false, from: 'Loading', to: 'Menu' }));
		this.stateManager.pushState('Menu', new Menu(CANVAS, CONTEXT, this.resources, false, { change: false, from: 'Menu', to: 'Play' }));
		this.stateManager.pushState('Play', new Play(CANVAS, CONTEXT, this.resources, false, { change: false, from: 'Play', to: 'Menu' }));
		this.stateManager.pushState('HighScore', new HighScore(CANVAS, CONTEXT, this.resources, false, { change: false, from: 'HighScore', to: 'Menu' }));
		
		return false;
	}

	/**
     * Starts the gameLoop
     */
	run() {	

		let deltaTime = this.clock;
		this.timeSinceLastUpdate += deltaTime ;
		//Controlling and stabilizing frames per second		
		while(this.timeSinceLastUpdate > this.timePerFrame) {
			this.timeSinceLastUpdate -= this.timePerFrame;
			//Update
			this.update(this.timePerFrame);
		}
		
		this.draw();
		clearInterval(this.clock);
		//Call run function recursively
		requestAnimFrame(this.run.bind(this));
	}

	/**
     * call update statemanager every frame
     *
     * @param {Number} deltaTime - Time per frame
     */
	update(deltaTime) {
		this.stateManager.update(deltaTime);
	}

	/**
     * call draw statemanager every frame
     */
	draw() {
		this.stateManager.draw();
	}

	/**
     * Listen to keyboard input  
     *
     * @param {object} e - object that provides Event properties.
     */
	keyboardInput(e) {
		this.stateManager.keyboardInput(e);
	}

	/**
     * Listen to mouse input 
     *
     * @param {object} e - object that provides Event properties.
     */
	mouseInput(e) {
		this.stateManager.mouseInput(e);
	}

	/**
     * Listen to touch input 
     *
     * @param {object} e - object that provides Event properties.
     */
	touchInput(e) {
		this.stateManager.touchInput(e);
	}

}

module.exports = GameLoop;