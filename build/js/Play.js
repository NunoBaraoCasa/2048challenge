/**
 * @ Play file, handle game logic, grid, pieces, animations, score gameover or win screen
 * @ author Nuno Barão <nunoandrebarao@outlook.pt>
 * @ version 1.0
 */

'use strict';

let Piece = require('../js/Piece.js');

module.exports = class Play {

	/**
     * The Play constructor
     *
     * @param {element} CANVAS - The HTML <canvas> element is used to draw graphics
     * @param {object} context - object that provides methods and properties for drawing on canvas.
     * @param {object} resources - The state resources containning all the images and sounds
     * @param {boolean} active - A flag to see if this state is active
     * @param {object} transition - Object that contains properties when, from and where to change state.
     */
	constructor(CANVAS, CONTEXT, resources, active, transition) {
		this.canvas = CANVAS;
		this.context = CONTEXT;
		this.resources = resources;
		this.active = active;
		this.gridSize = 4;
		this.slot = {
			empty: 0,
		};
		this.moveDirection;
		this.direction = {
			right: 1,
			left: -1,
			up: -1,
			down: 1,
		};
		this.moveOrientation;
		this.orientation = {
			horizontally: 0,
			vertically: 1,
		}
		this.touch = {
			startX: 0,
			startY: 0,
			endX: 0,
			endY: 0,
		}
		this.swipe = {
			left: 0,
			right: 1,
			up: 2,
			down: 3,
		}
		this.grid = [];
		this.tempGrid;
		this.emptyPieces = [];

		this.keyState = {};
		this.keyboard = {
			left:37,
			up:38,
			right:39,
			down:40,
		}
		this.is_toMove = false;
		this.is_toUpdateEmpty = false;
		this.score = 0;
		this.transition = transition;
		this.is_gameOver = false;
		this.is_toChange = false;
		this.is_toAnimate = false;
		this.is_toDraw =true;
		this.gridChanged = false;

		this.logo = new Image();
		this.logo.src = 'images/splash_logo.png';

		this.setupGrid();
		//Add first two pieces
		this.addPiece();
		this.addPiece();
	}

	/**
     * Setup our grid, add all pieces to grid 2d array and 
     * add all empty pieces to emptyPieces array
     *
     * grid = [ 
     *     [piece, piece, piece, piece]
     *     [piece, piece, piece, piece]
     *     [piece, piece, piece, piece]
     *	   [piece, piece, piece, piece]
     * ];
     *
     * emptyPieces = [piece, piece, piece, piece, piece ...];
     */
	setupGrid() {
		//Setup Grid
		for(let y = 0; y < this.gridSize; y++) {
			this.grid[y] = [];
			for(let x = 0; x < this.gridSize; x++) {
				//Create new Piece 
				this.grid[y][x] = new Piece(x, y, 0, '#000', x, y);
				this.emptyPieces.push(this.grid[y][x].pos);
			}
		}

		return false;
	}

	/**
     * update the game every frame based on player input 
     *
     * @param {number} deltaTime - Frames per second
     * @returns {boolean} - true or false
     */
	update(deltaTime) {
		
		if(this.is_toMove) {
			this.movePieces(this.moveDirection, this.moveOrientation);
			this.checkChanges();
			this.checkWinOrLose();
		}

		this.updatePiecesAnimation(deltaTime);
		
		if(this.is_toAddPiece)
			this.addPiece();

		//Return transition.change to check if we need to change state
		return this.transition.change;
	}

	 /**
     * Call functions to move pieces horizontally or vertically
     *
     * @param {Object} direction - Object that contains direction properties. (left, right)
     * @param {Object} orientation - Object thata provides orientation properties. (horizontally, vertically)
     * @returns {boolean} is_toMove - true or false.  
     */
    movePieces(direction, orientation) {
		if(orientation == this.orientation.horizontally)
			this.moveHorizontaly(direction, orientation);
		else
			this.moveVertically(direction, orientation);

		return this.is_toMove = false;
	}

	/**
     * Starts te proces to move pieces horizontally
     *
     * @param {Object} direction - Object that contains direction properties. (left, right)
     * @param {Object} orientation - Object thata provides orientation properties. (horizontally, vertically)
     * @returns {} 
     */
    moveHorizontaly(direction, orientation) {
    	for(let y = 0; y < this.gridSize; y++) {
    		let row = this.grid[y];
			this.grid[y] = this.handleRow(direction, row, orientation);
		}
		return false;
    }

    /**
     * Starts te proces to move pieces vertically
     *
     * @param {Object} direction - Object that contains direction properties. (left, right)
     * @param {Object} orientation - Object thata provides orientation properties. (horizontally, vertically)
     * @returns {} 
     */
    moveVertically(direction, orientation) {
    	for(let x = 0; x < 4; x++){
    		let column = this.getColumn(this.grid, x);
			column = this.handleRow(direction, column, orientation);
    		for(let y = 0; y < 4; y++){
    			this.grid[y][x] = column[y];
    		}
    	}
    	return false;
    }

    /**
     * Handle row process, sum pieces and move them to the right place
     *
     * @param {Object} direction - Object that contains direction properties. (left, right)
     * @param {Array} arr - Array of pieces
     * @param {Object} orientation - Object thata provides orientation properties. (horizontally, vertically)
     * @returns {Array} mergedRows - array of pieces.
     */
    handleRow(direction, arr, orientation) {
    	//Get two arrays [0, 0] [2, 2] and return them merged [0, 0, 2, 2]
    	let rowToSum = this.manipulateRow(direction, arr);
    	//Reverse given array based on direction
    	let flipRow = this.flipRowByOrientation(direction, rowToSum);
    	//sum pieces values together if they got the same value
    	let rowSummed = this.sum(flipRow);

    	//Reverse given array based on direction
    	flipRow = this.flipRowByOrientation(direction, rowSummed);
    	//Get two arrays [0, 0] [2, 2] and return them merged [0, 0, 2, 2]
    	let mergedRows = this.manipulateRow(direction, flipRow);
    	//Update the position of every piece based on orientation
    	let updatePiecesPos = this.updatePiecesPosition(mergedRows, orientation);
    	//Update Pieces Direction
    	this.updatePiecesDirection(mergedRows, direction);

    	return mergedRows;
    }

    /**
     * Get a column out of grid and stores into an array
     *
     * @param {Array} grid - Array of objects that contains pieces
     * @param {Number} col - number of column index
     * @returns {Array} column - array of pieces.
     */
    getColumn(grid, col) {
    	let column = [];
       	for(let i = 0; i < this.gridSize; i++){
        	column.push(grid[i][col]);
       	}
       	return column;
    }

    /**
     * Get two arrays [0, 0] [2, 2] and return them merged [0, 0, 2, 2]
     *
     * @param {array} arr - array that contains pieces objects. [pice.value, piece.value, piece.value, piece.value] 
     * @returns {array} a function that merge two rows anda returns them merged as an array.
     */
    manipulateRow(direction, arr) {
    	let zerosRow = this.RowWithZeros(arr);
    	let notZerosRow = this.rowWithoutZeros(arr);

    	return this.mergeRows(direction, notZerosRow, zerosRow);
    }

     /**
     * Create a new array with pieces only with value zero, extracted from the array given.
     * i.e: givenArray [0, 0, 2, 2] newArray [0, 0]
     *
     * @param {array} arr - array that contains pieces objects. [pice.value, piece.value, piece.value, piece.value] 
     * @returns {Array} zerosRow - array of pieces.
     */
    RowWithZeros(arr) {
    	let zerosRow = arr.filter((n) =>  n.value == 0 );
    	return zerosRow;
    }

    /**
     * Create a new array with pieces only with value greater then zero, extracted from the array given.
     * i.e: givenArray [0, 0, 2, 2] newArray [2, 2]
     *
     * @param {array} arr - array that contains pieces objects. [pice.value, piece.value, piece.value, piece.value] 
     * @returns {Array} rowToSum - array of pieces.
     */
    rowWithoutZeros(arr) {
		let rowToSum = arr.filter((n) => n.value > 0);
		return rowToSum;
    }

    /**
     * Reverse given array based on direction 
     *
     * @param {object} direction - object that contains directions properties. (left right)
     * @param {array} arr - array that contains pieces objects. [pice.value, piece.value, piece.value, piece.value] 
     * @returns {Array} arr - array of pieces.
     */
    flipRowByOrientation(direction, arr) {
    	if(direction == this.direction.right)
    		return arr.reverse();

    	return arr;
    }

	/**
     * sum two pieces values together if they got the same value
     *
     * @param {array} row - array that contains 4 pieces to be summed. i.e: row = [piece, piece, piece, piece] .
     * @returns {Array} row - array of pieces.
     */
    sum(row) {
    	//Iterate through row
    	for(let i = 0; i < row.length - 1; i++) {
    		let pieceA = row[i];
    		//If pieceA value is not 0 and pieceB its an element of array
    		if(i + 1 <= row.length && pieceA.value != 0) {
    			let pieceB = row[i + 1];
    			//If pieceA value its euqal to pieceB value then sum
    			if(pieceA.value == pieceB.value) {
    				pieceA.value += pieceB.value;
    				pieceB.value = 0;
    				pieceA.updateColor(pieceA);
    				this.updateScore(pieceA.value);
    				this.gridChanged = true;
    			}
    		}
    	}

    	return row;
    }

    /**
     * Update the position of every piece based on orientation
     *
     * @param {Array} arr - Array of objects that contains pieces
     * @param {object} orientation - Object that as orientation properties (horizontally, vertically)
     */
    updatePiecesPosition(arr, orientation) {
		for(let i = 0; i < arr.length; i++){
			orientation == this.orientation.horizontally ? arr[i].pos.set(i, arr[i].pos.y) : arr[i].pos.set(arr[i].pos.x, i);
		}
		return false;
    }

    /**
     * Update Pieces Direction
     *
     * @param {Array} arr - Array of objects that contains pieces
     * @param {object} direction - Object that provides dierction properties. (left, right)
     */
    updatePiecesDirection(arr, direction) {
    	for(let i = 0; i < arr.length; i++){
			arr[i].setDirection(direction);
		}
		return false;
    }

    /**
     * Merge two rows, row of pieces with value zero [0, 0] with row of pieces with values to be summed [2, 2]
     * based on direction.   [0, 0, 2, 2] or [2, 2, 0, 0]
     *
     * @param {object} direction - object that provide direction properties (left or right).
     * @param {array} rowToSum - array that contains pieces objects with values to be summed.  [2, 2] = [pice.value, piece.value] 
     * @param {array} zerosRow - array that contains pieces objects with value zero. [0, 0] = [piece.value, piece.value]
     */
    mergeRows(direction, rowToSum, zerosRow) {
    	if(direction == this.direction.left)
    		return rowToSum.concat(zerosRow);
    	else
    		return zerosRow.concat(rowToSum);
    }

	/**
     * Update score 
     *
     * @param {number} points - points from pieces sum, to add to score .
     */
    updateScore(points) {
    	this.score += points;
    	localStorage.setItem("HighScore", this.score);
    	return this.score;
    }

    /**
     * Check if there was a change on grid 
     *
     */
    checkChanges() {
		this.emptyPieces = [];
		for(let y = 0; y < this.gridSize; y++) {
			for(let x = 0; x < this.gridSize; x++) {
				let piece = this.grid[y][x];
				
				//Check if piece as moved
				let pieceAsMoved = this.checkIfPieceMoved(piece);
				if(pieceAsMoved)
					piece.setPieceAnimation(piece);

				//Update oldPos of piece to the new pos
				piece.oldPos.set(piece.pos.x, piece.pos.y);
				
				if(piece.value == 0)
					this.emptyPieces.push(piece.pos);
			}
		}
		return false;
	}

	/**
     * Check if piece as moved 
     *
     * @param {object} piece - properties of piece .
     */
	checkIfPieceMoved(piece) {
		//if old position not equal to atcual position, then piece as moved
		if(piece.pos.x != piece.oldPos.x || piece.pos.y != piece.oldPos.y){
			this.gridChanged = true;
			return true;
		}

		return false;
	}

	/**
     * Check if the player won or lose
     *
     */
	checkWinOrLose() {
		//Iterate through grid
		for(let y = 0; y < this.gridSize; y++) {
			for(let x = 0; x < this.gridSize; x++) {
				let piece = this.grid[y][x];
				
				//If Player reach 2048 wins
				if(this.checkIfPlayerWins(piece))
					return false;
				//If piece value are 0, then return
				if(this.checkEmptyPiece(piece))
					return false;
				//If x and y are elements of array 
				//Prevent checking elemnts outside grid i.e: this.grid[y][x + 1] when x is last element
				if(x < (this.gridSize - 1) && y < (this.gridSize - 1))
					//return false;
				//Check If we got vertical moves
				if(x < (this.gridSize - 1) && y < (this.gridSize - 1) && this.checkVerticalMoves(piece, this.grid[y + 1][x], x, y))
					return false;
				//Check If we got horizontal moves
				if(x < (this.gridSize - 1) && y < (this.gridSize - 1) && this.checkHorizontalMoves(piece, this.grid[y][x + 1], x, y))
					return false;
				
			}
		}
		//If we hadn´t return until here, then its game over
		this.storeScore();
		this.is_gameOver = true;
	}

	/**
     * Check if the player reached 2048
     *
     * @param {object} piece - Contains piece properties
     * @returns {Boolean} 
     */
	checkIfPlayerWins(piece) {
		//If player had reach 2048 
		if(piece.value == 2048) {
			this.drawWinOrLose("YOU WIN");
			return true;
		}
		return false;
	}

	/**
     * Check for piece with value 0 and continue iterate
     *
     * @param {object} piece - Contains piece properties
     * @returns {Boolean} 
     */
	checkEmptyPiece(piece) {
		//If piece is a empty piece return
		if(piece.value == 0)
			return true;

		return false;
	}

	/**
     * Check for available vertical moves
     *
     * @param {object} piece - Contains piece properties
     * @param {Number} x - position x on grid
     * @param {Number} y - position y on grid
     * @returns {Boolean} 
     */
	checkVerticalMoves(piece, bottomPiece, x, y) {
		if(piece.value == bottomPiece.value)
			return true;

		return false;
	}

	/**
     * Check for available horizontal moves
     *
     * @param {object} piece - Contains piece properties
     * @param {Number} x - position x on grid
     * @param {Number} y - position y on grid
     * @returns {Boolean} false
     */
	checkHorizontalMoves(piece, rightPiece,x, y) {
		if(piece.value == rightPiece.value) 
			return true;

		return false;	
	}

	/**
     * Update animation of pieces
     *
     * @param {Number} deltaTime - Time per Frame 
     */
	updatePiecesAnimation(deltaTime) {
		let nothingToAnimate = true;
		for(let y = 0; y < this.gridSize; y++) {
			for(let x = 0; x < this.gridSize; x++) {
				let piece = this.grid[y][x];

				if(piece.value == this.slot.empty)
					continue;
				
				if(piece.animateFrom.x == piece.animateTo.x && piece.animateFrom.y == piece.animateTo.y)
					continue;

				nothingToAnimate = false;

				let newX = piece.animateFrom.x += (piece.direction * piece.speed) * deltaTime;
				let newY = piece.animateFrom.y += (piece.direction * piece.speed) * deltaTime;
				piece.animateFrom.set(newX, newY);

				switch(piece.direction) {
					case this.direction.right || this.direction.down: 
						if(piece.animateFrom.x >= piece.animateTo.x)
							piece.animateFrom.x = piece.animateTo.x;

						if(piece.animateFrom.y >= piece.animateTo.y)
							piece.animateFrom.y = piece.animateTo.y;
						break;
					case this.direction.left || this.direction.up: 
						if(piece.animateFrom.x <= piece.animateTo.x)
							piece.animateFrom.x = piece.animateTo.x;

						if(piece.animateFrom.y <= piece.animateTo.y)
							piece.animateFrom.y = piece.animateTo.y;
						break;
				}
			}
		}

		if(nothingToAnimate && this.gridChanged){
			this.is_toAddPiece = true;
			this.gridChanged = false;
		}

		return false;
	}

	/**
     * Add new piece with random position and value to grid
     */
	addPiece() {
		let randomIndex = Math.floor(Math.random()*this.emptyPieces.length);	
		let randomEmptyPiece = this.emptyPieces[randomIndex];
		let randomPieceValue = this.randomNumber(0,4);
		let piece = this.grid[randomEmptyPiece.y][randomEmptyPiece.x];

		randomPieceValue > 1 ? piece.value = 2 : piece.value = 4;
		piece.pos.set(randomEmptyPiece.x, randomEmptyPiece.y);
	
		piece.updateColor(piece);
		piece.setPieceAnimation(piece);
		
		this.emptyPieces.splice(randomIndex, 1);
		
		this.is_toAddPiece = false;
			
		this.checkChanges();
		this.checkWinOrLose();

		return false;
	}

	/**
     * Generate a random Number between a range (min-max) given
     *
     * @param {Number} min - Min Number
     * @param {Number} max - Max Number
     * @returns {Number} A random Number.  
     */
	randomNumber(min, max) {
		return Math.floor(Math.random() * max) + min;
	}

	/**
     * draw the game every frame
     */
	draw() {
		this.clearCanvas();
		this.drawBackground();	
		this.drawPieces();
		this.drawLogo();
		this.drawScore();

		if(this.is_gameOver)
			this.drawWinOrLose();

		this.is_toDraw = false;
	}

	/**
     * Clear our canvas for the next frame
     *
     */
	clearCanvas() {
		this.context.clearRect(0, 0, canvas.width, canvas.height);
	}

	/**
     * Draw Background tiles and grid tiles
     */
    drawBackground() {
		// Background Grid!!
		let offset = 4;
		for(let y = 0; y < this.gridSize; y++) {
			for(let x = 0; x < this.gridSize; x++) {
				let leftOffset = 4;
				let topOffset = 200;

				//Drawing Background tiles
				this.context.fillStyle = '#333';
				this.context.fillRect(
					leftOffset  + (x * (this.grid[y][x].width + offset)),
					topOffset  + (y * (this.grid[y][x].height + offset)),
					95, 95);
			}
		}
    }

    /**
     * Draw pieces on canvas
     */
	drawPieces() {
		for(let y = 0; y < this.gridSize; y++) {
			for(let x = 0; x < this.gridSize; x++) {
				let piece = this.grid[y][x];

				if(piece.value == this.slot.empty)
					continue;
				
				//Draw piece
				piece.draw(piece, this.context);

			}
		}
	}

	drawLogo() {
		//Draw logo image
		this.context.drawImage(this.logo, (this.canvas.width / 2) - this.logo.width / 2, 50);
	}

	/**
     * Draw Score on canvas
     */
	drawScore() {
		this.context.fillStyle = '#333' ;
		this.context.font = "28px Arial";
		this.context.fillText(
			"score: " + this.score,
		 	canvas.width / 2,
		 	170);
	}

	/**
     * Call functions to move pieces horizontally or vertically
     *
     * @param {string} text - Text to be drawn on tile
     */
	drawWinOrLose(text) {
		this.context.fillStyle = 'rgba(0, 0, 0, 0.8)';
		this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

		if(this.is_gameOver){
			this.resources.sprites['gameOver'].setPosition((this.canvas.width/2) - (this.resources.sprites['gameOver'].width / 2), 100);
			this.resources.sprites['gameOver'].draw(0, this.context);
		}else{
			this.resources.sprites['youWin'].setPosition((this.canvas.width/2) - (this.resources.sprites['youWin'].width / 2), 100);
			this.resources.sprites['youWin'].draw(0, this.context);
		}

		this.context.fillStyle = '#fff' ;
		this.context.font = "38px Arial";
		this.context.fillText(
			"SCORE: " + this.score,
		 	canvas.width / 2,
		 	200);

		this.resources.sprites['backToMenu'].draw(0, this.context);
		
	}

	/**
     * Store Score locally 
     */
	storeScore() {
		localStorage.setItem("HighScore", this.score);
	}

	/**
     * Listen to keyboard input
     *
     * @param {object} e - object that provides Event properties.
     */
	keyboardInput(e) {
		this.keyState[e.keyCode || e.which] = true;
		this.handleInput(e.keyCode, this.keyboard);
		this.keyState[e.keyCode || e.which] = false;
	}

	/**
     * Listen to mouse input 
     *
     * @param {object} e - object that provides Event properties.
     */
	mouseInput(e) {
		//Get x and y position of mouse click on canvas
		let mousePosition = this.getMousePos(e);

	    if (this.isInside(mousePosition,this.resources.sprites['backToMenu']) && this.is_gameOver) {
	    	this.resetEverything();
			this.transition.change = true;
	    } 
	}

	/**
     * Listen to touch input 
     * 
     * @param {object} e - object that provides Event properties
     */
    touchInput(e) {

    	//Get x and y position of mouse click on canvas
		let touchPosition = this.getMousePos(e);
		if(e.changedTouches[0].pageX == "undefined")
            touchPosition = this.getTouchPos(this.canvas, e);
        else
            touchPosition = this.getTouchPosAndroid(this.canvas, e);

	    if (this.isInside(touchPosition,this.resources.sprites['backToMenu'])) {
	    	this.resetEverything();
			this.transition.change = true;
	    }
        
        if(e.type == "touchstart") {
            this.touch.startX = touchPosition.x;
            this.touch.startY = touchPosition.y;
        }else{
            this.touch.endX = touchPosition.x;
            this.touch.endY = touchPosition.y;
        }
        
        let xDirection = this.touch.startX < this.touch.endX ? this.swipe.right : this.swipe.left;
        let yDirection = this.touch.startY < this.touch.endY ? this.swipe.down : this.swipe.up;
        
        let swipeX = Math.abs(this.touch.startX - this.touch.endX);
        let swipeY = Math.abs(this.touch.startY - this.touch.endY);
        
        let swipeDirection = swipeX < swipeY ? yDirection : xDirection;  
        
        if(e.type == "touchend"){
            this.handleInput(swipeDirection, this.swipe);
        }
    }

    /**
     * Handle input 
     * 
     * @param {object} direction - object that provides Event properties
     * @param {object} inputType - object that provides input properties
     */
    handleInput(direction, inputType) {
    	switch(direction) {
			case inputType.left:
				this.moveOrientation = this.orientation.horizontally;
				this.moveDirection = this.direction.left;
				this.is_toMove = true;
				this.is_toDraw = true;
				break;
			case inputType.up:
				this.moveOrientation = this.orientation.vertically;
				this.moveDirection = this.direction.up;
				this.is_toMove = true;
				this.is_toDraw = true;
				break;
			case inputType.right:
				this.moveOrientation = this.orientation.horizontally;
				this.moveDirection = this.direction.right;
				this.is_toMove = true;
				this.is_toDraw = true;
				break;
			case inputType.down:
				this.moveOrientation = this.orientation.vertically;
				this.moveDirection = this.direction.down;
				this.is_toMove = true;
				this.is_toDraw = true;
				break;
			default:
				break;
		}
    }

	/**
     * Get mouse x and y position on canvas
     *
     * @param {object} e - object that provides Event properties.
     * @returns {object} - object with position properties.
     */
	getMousePos(e) {
	    var rect = this.canvas.getBoundingClientRect();
	    return {
	        x: e.clientX - rect.left,
	        y: e.clientY - rect.top
	    };
	}

	/**
     * Get touch x and y position on canvas
     *
     * @param {object} e - object that provides Event properties.
     * @returns {object} - object with position properties.
     */
    getTouchPos(canvas, e) {
        let button = this.canvas.getBoundingClientRect();
        return {
            x: Math.round((e.pageX - button.left) / (button.right - button.left) * this.canvas.width),
            y: Math.round((e.pageY - button.top) / (button.bottom - button.top) * this.canvas.height)
        };
    }
    
    /**
     * Get android touch x and y position on canvas
     *
     * @param {object} e - object that provides Event properties.
     * @returns {object} - object with position properties.
     */
    getTouchPosAndroid(canvas, e) {
        let button = this.canvas.getBoundingClientRect();
        return {
            x: Math.round((e.changedTouches[0].pageX - button.left) / (button.right - button.left) * this.canvas.width),
            y: Math.round((e.changedTouches[0].pageY - button.top) / (button.bottom - button.top) * this.canvas.height)
        };
    }

	/**
     * Check if the click was inside a button. i.e: Play or highScore button 
     *
     * @param {object} position - object that provides properties of click position (x, y) .
     * @param {object} button - object that provides properties of the button (x, y, width, height).
     * @returns {boolean} - true or false
     */
	isInside(position, button){
		//Return true if was clicked inside
	    return position.x > button.x && position.x < button.x + button.width
	    	   && position.y < button.y + button.height && position.y > button.y;
	}

	/**
     * Reset every variable, to start new game
     */
	resetEverything() {
		this.grid = [];
		this.is_toMove = false;
		this.is_toUpdateEmpty = false;
		this.score = 0;
		this.is_gameOver = false;
		this.is_toChange = false;
		this.is_toDraw =true;
		this.setupGrid();
		//Add first two pieces
		this.addPiece();
		this.addPiece();

		return false;
	}

	/**
     * Set this state (Play) active to true or false
     *
     * @param {boolean} active - A flag to know if its true or false.
     */
	setActive(active) {
		return this.active = active;
	}
}