/**
 * @ StateManager file, manage states (splashscreen, loading, menu, play)
 * @ author Nuno Barão <nunoandrebarao@outlook.pt>
 * @ version 1.0
 */

'use strict';

module.exports = class StateManager {

    /**
     * The StateManager constructor
     *
     * @param {object} CONTEXT - object that provides methods and properties for drawing on the canvas.
     */
	constructor(CONTEXT) {
		this.context = CONTEXT;
		this.stack = [];
		this.states = [
			{ change: false, from: 'SplashScreen', to: 'Play' },
		];
	}

    /**
     * Update the active state in stack[] (splashscreen, loading, menu, play)
     *
     * @param {number} deltaTime - Time per frame.
     */
	update(deltaTime) {
		for(let stateId in this.stack) {
			if(this.stack[stateId].active) {
				let changeState = this.stack[stateId].update(deltaTime);
				if(changeState) {
					let transition = this.stack[stateId].transition;
					this.stack[transition.from].setActive(false);
					this.stack[transition.from].transition.change = false;
					this.stack[transition.to].setActive(true);
				}
			}
		}
	}

	/**
	 * Draw the active state in stack[] (splashScreen, loading, menu, play)
     */
	draw() {
		for(let stateId in this.stack) {
			if(this.stack[stateId].active) {
				this.stack[stateId].draw();
			}
		}
	}

	/**
     * Listen to keyboard input of active state in stack[] (splashscreen, loading, menu, play)
     *
     * @param {object} e - object that provides Event properties.
     */
	keyboardInput(e) {
		for(let stateId in this.stack) {
			if(this.stack[stateId].active) {
				this.stack[stateId].keyboardInput(e);
			}
		}
	}

	/**
     * Listen to mouse input of active state in stack[] (splashscreen, loading, menu, play)
     *
     * @param {object} e - object that provides Event properties.
     */
	mouseInput(e) {
		console.log(this.stack);
		for(let stateId in this.stack) {
			if(this.stack[stateId].active) {
				this.stack[stateId].mouseInput(e);
			}
		}
	}

	/**
     * Listen to touch input of active state in stack[] (splashscreen, loading, menu, play)
     *
     * @param {object} e - object that provides Event properties.
     */
	touchInput(e) {
		for(let stateId in this.stack) {
			if(this.stack[stateId].active) {
				this.stack[stateId].touchInput(e);
			}
		}
	}

	/**
     * Push a State to stack[]
     *
     * @param {string} stateId - A string with de name os state i.e: "Play".
     * @param {object} state - A state i.e: Play.
     */
	pushState(stateId, state) {
		this.stack[stateId] = (state);
	}

	/**
     * Pop a state from stack[]
     *
     * @param {string} stateId - A string with de name os state i.e: "Play".
     */
	popState(stateId) {
		delete this.stack[stateId];
	}
}