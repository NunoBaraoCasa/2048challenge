/**
 * @ Piece file
 * @ author Nuno Barão <nunoandrebarao@outlook.pt>
 * @ version 1.0
 */

'use strict';

let Vec2 = require('../js/Vec2.js');

module.exports = class Piece {

	/**
     * The Piece constructor
     *
     * @param {Number} x - x position on grid
     * @param {Number} y - y position on grid
     * @param {Number} value - Contains the piece value.
     * @param {String} color - Contians the piece color. i.e:('#000')
     * @param {Number} oldX - old x position on grid.
     * @param {Number} oldY - old y position on grid.
     */
	constructor(x, y, value, color, oldX, oldY) {
		this.pos = new Vec2(x, y);
		this.value = value;
		this.color = color;
		this.oldPos = new Vec2(oldX, oldY);
		this.animateFrom = new Vec2(0, 0);
		this.animateTo = new Vec2(200, 0);
		this.width = 95;
		this.height = 95;
		this.leftOffset = 0;
		this.topOffset = 196;
		this.inbetweenOffset = 4;
		this.speed = 50;
		this.direction = 0;
		this.colors = {
			2: '#eee4da',
			4: '#eee1c9',
			8: '#f78e48',
			16: '#fc5e2e',
			32: '#ff3333',
			64: '#ff0000',
			128: '#ffff80',
			256: '#ffff33',
			512: '#ffdb4d',
			1024: '#ffd11a',
			2048: '#ff9900',
		};
	}

	/**
     * Update piece
     *
     * @param {Number} deltaTime - Time per frame
     */
	update(deltaTime) {

	}

	/**
     * Update piece color based on its value 
     *
     * @param {object} piece - object that provides properties of our piece (position, color, value ...).
     * @returns {object} piece - object that provides properties of our piece (position, color, value ...).
     */
	updateColor(piece) {
		piece.color = this.colors[piece.value];

		return piece;
	}

	/**
     * Draw piece
     */
	draw(piece, context) {
		//Drawing numbered/colored tiles
		context.fillStyle = '' + piece.color;
		context.fillRect(piece.animateFrom.x, piece.animateFrom.y, piece.width, piece.height);

		this.drawNumber(piece, piece.pos.x, piece.pos.y, context);
	}

	/**
     * Draw number on pieces
     *
     * @param {Object} piece - Object that contains piece properties. (position, value, color...)
     * @param {Number} x - Position x 
     * @param {Number} y - Position y  
     */
	drawNumber(piece, x, y, context) {
		let fontSizes = ['60px', '60px', '50px', '40px'];
		let fontColor = '#fff';
		
		if(piece.value <= 4)
			fontColor = '#333';

		//Drawing Numbers inside colored tiles
		context.fillStyle = fontColor;
		context.font = fontSizes[piece.value.toString().length - 1] + " Arial";
		context.textAlign = "center"; 
		context.textBaseline = "middle";
		context.fillText(
			piece.value, piece.animateFrom.x + piece.width/2, piece.animateFrom.y + piece.height/2);

	}

	/**
     * Set position where animation starts
     *
     * @param {Number} x - position on canvas
     * @param {Number} y - position on canvas
     */
	setAnimateFrom (x, y) {
		this.animateFrom.set(x, y);
	}

	/**
     * Set position where animation ends
     *
     * @param {Number} x - position on canvas
     * @param {Number} y - position on canvas
     */
	setAnimateTo(x, y) {
		this.animateTo.set(x, y);
	}

	/**
     * Check if piece as moved 
     *
     * @param {object} piece - properties of piece .
     */
	setPieceAnimation(piece) {
		//Animation X and Y From where animation should start
		let xFrom = piece.leftOffset + piece.inbetweenOffset + (piece.oldPos.x * (piece.width + piece.inbetweenOffset));
		let yFrom = piece.topOffset + piece.inbetweenOffset + (piece.oldPos.y * (piece.height + piece.inbetweenOffset));

		//Animation X and Y From where animation should end
		let xTo = piece.leftOffset + piece.inbetweenOffset + (piece.pos.x * (piece.width + piece.inbetweenOffset));
		let yTo = piece.topOffset + piece.inbetweenOffset + (piece.pos.y * (piece.height + piece.inbetweenOffset));

		//Set positions
		piece.animateFrom.set(xFrom, yFrom);
		piece.animateTo.set(xTo, yTo);
	}

	/**
     * Set direction 
     *
     * @param {object} direction - piece direction
     */
	setDirection(direction) {
		return this.direction = direction;
	}
}