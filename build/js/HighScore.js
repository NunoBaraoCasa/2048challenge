/**
 * @ Highscore file, highscore state
 * @ author Nuno Barão <nunoandrebarao@outlook.pt>
 * @ version 1.0
 */

'use strict';

module.exports = class HighScore{

	/**
     * The Highscore constructor
     *
     * @param {element} CANVAS - The HTML <canvas> element is used to draw graphics
     * @param {object} context - object that provides methods and properties for drawing on canvas.
     * @param {object} resources - The state resources containning all the images and sounds
     * @param {boolean} active - A flag to see if this state is active
     * @param {object} transition - Object that contains properties when, from and where to change state.
     */
	constructor(CANVAS, CONTEXT, resources, active, transition) {
		this.canvas = CANVAS;
		this.context = CONTEXT;
		this.active = active;
		this.resources = resources;
		this.transition = transition;
		this.highScore = localStorage.getItem("HighScore");
	}

	/**
     * Update Highscore
     *
     * @param {Number} deltaTime - Time per frame
     * @return {Boolean} - true or false
     */
	update(deltaTime) {
		this.highScore = localStorage.getItem("HighScore");
		//Return transition.change to check if we need to change state
		return this.transition.change;
	}

	/**
     * Draw Higscore and button back to menu on canvas
     */
	draw() {
		this.clearCanvas();
		this.drawHighscore();
		this.drawButtons();

		return false;
	}

	/**
     * Clear our canvas for the next frame
     */
	clearCanvas() {
		this.context.clearRect(0, 0, canvas.width, canvas.height);
		return false;
	}

	/**
     * Draw Highscore to canvas
     */
	drawHighscore() {
		//Draw HighScore title, (reused sprite from button)
		this.resources.sprites['buttonHighScores'].setPosition(
			(this.canvas.width/2) - (this.resources.sprites['buttonHighScores'].width / 2), 100);
		this.resources.sprites['buttonHighScores'].draw(0, this.context);
		
		//Draw score
		let score = (this.highScore === null || typeof this.highScore === 'undefined') ? 0 : this.highScore;
		
		this.context.fillStyle = '#fff' ;
		this.context.font = "38px Arial";
		this.context.fillText("BEST: " + score, canvas.width / 2, 200);

		return false;
	}

	/**
     * Draw Button to canvas
     */
	drawButtons() {
		//Set button position
		this.resources.sprites['backToMenu'].setPosition(
			(this.canvas.width/2) - (this.resources.sprites['backToMenu'].width / 2),
			500);
		//Draw button
		this.resources.sprites['backToMenu'].draw(0, this.context);

		return false;
	}

	/**
     * Listen to keyboard input  
     *
     * @param {object} e - object that provides Event properties.
     */
	keyboardInput(e) {
		
	}

	/**
     * Listen to mouse input 
     *
     * @param {object} e - object that provides Event properties.
     */
	mouseInput(e) {
		//Get x and y position of mouse click on canvas
		let mousePosition = this.getMousePos(e);
		//If click was inside button then make transition.change true to change to Menu state
		this.isInside(mousePosition,this.resources.sprites['backToMenu']) ? this.transition.change = true : '';
	}

	/**
     * Listen to touch input 
     * 
     * @param {object} e - object that provides Event properties
     */
    touchInput(e) {
        let touchPosition;
        if(e.changedTouches[0].pageX == "undefined")
            touchPosition = this.getTouchPos(this.canvas, e);
        else
            touchPosition = this.getTouchPosAndroid(this.canvas, e);
        
        //If touch was inside button then make transition.change true to change to Menu state
		this.isInside(touchPosition,this.resources.sprites['backToMenu']) ? this.transition.change = true : '';
		
    }

	/**
     * Get mouse x and y position on canvas
     *
     * @param {object} e - object that provides Event properties.
     * @returns {object} - object with position properties.
     */
	getMousePos(e) {
	    var button = this.canvas.getBoundingClientRect();
	    return {
	        x: e.clientX - button.left,
	        y: e.clientY - button.top
	    };
	}

	/**
     * Get touch x and y position on canvas
     *
     * @param {object} e - object that provides Event properties.
     * @returns {object} - object with position properties.
     */
    getTouchPos(canvas, e) {
        let button = this.canvas.getBoundingClientRect();
        return {
            x: Math.round((e.pageX - button.left) / (button.right - button.left) * this.canvas.width),
            y: Math.round((e.pageY - button.top) / (button.bottom - button.top) * this.canvas.height)
        };
    }
    
    /**
     * Get android touch x and y position on canvas
     *
     * @param {object} e - object that provides Event properties.
     * @returns {object} - object with position properties.
     */
    getTouchPosAndroid(canvas, e) {
        let button = this.canvas.getBoundingClientRect();
        return {
            x: Math.round((e.changedTouches[0].pageX - button.left) / (button.right - button.left) * this.canvas.width),
            y: Math.round((e.changedTouches[0].pageY - button.top) / (button.bottom - button.top) * this.canvas.height)
        };
    }

	/**
     * Check if the click was inside a button. i.e: Play or highScore button 
     *
     * @param {object} position - object that provides properties of click position (x, y) .
     * @param {object} button - object that provides properties of the button (x, y, width, height).
     * @returns {boolean} - true or false
     */
	isInside(position, button){
		//Return true if was clicked inside
	    return position.x > button.x && position.x < button.x + button.width
	    	   && position.y < button.y + button.height && position.y > button.y;
	}

	/**
     * Set this state (splashscreen) active to true or false
     *
     * @param {boolean} active - A flag to know if its true or false.
     */
	setActive(active) {
		return this.active = active;
	}

	/**
     * Get score
     */
	getScore() {
		return this.highScore;
	}

	/**
     * Set score
     *
     * @param {Number} score - contians score number
     */
	setScore(score) {
		localStorage.setItem("HighScore", score);
	}
}
