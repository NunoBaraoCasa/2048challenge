/**
 * @ Loading file, Load resources (images, audio) until they are ready
 * @ author Nuno Barão <nunoandrebarao@outlook.pt>
 * @ version 1.0
 */

 'use strict';

let GameLoop = require('../js/GameLoop.js');

module.exports = class Loading {

	/**
     * The Loading constructor
     *
     * @param {element} CANVAS - The HTML <canvas> element is used to draw graphics
     * @param {object} context - object that provides methods and properties for drawing on canvas.
     * @param {object} resources - The state resources containning all the images and sounds
     * @param {boolean} active - A flag to see if this state is active
     * @param {object} transition - Object that contains properties when, from and where to change state.
     */
	constructor(CANVAS, CONTEXT, resources, active, transition) {
		this.canvas = CANVAS;
		this.context = CONTEXT;
		this.resources = resources;
		this.active = active;
		this.transition = transition;

		this.is_toLoadImages = true;
		this.is_toLoadSounds = false;
		self.imagesAreReady = false;
		self.soundsAreReady = true;

		this.splashImage = new Image();
		this.splashImage.src = 'images/splash_logo.png';
		
		this.is_toDelay = true;
		self.progressBar = {
			width: 300,
			height: 5,
			totalToLoad: this.resources.imagesSources.length + this.resources.soundsSources.length,
			totalLoaded: 0,
		};
	}

	/**
     * Update Menu
     *
     * @param {Number} deltaTime - Time per frame
     * @return {Boolean} - true or false
     */
	update(deltaTime) {
		//Load Resources
		this.loadResources();
		//Check if resources are ready and loaded
		this.resourcesReady();
		//Return transition.change to check if we need to change state
		return this.transition.change;
	}

	 /**
     * Load resources
     */
	async loadResources() {
		if(this.is_toDelay)
			await this.sleep(3000);

		if(this.is_toLoadImages)
			this.loadImages();

		if(this.is_toLoadSounds)
			this.loadSounds();
	}

	/**
     * Check if resources are loaded and ready to add to sprites
     */
	resourcesReady() {
		//If resources are ready and loaded 
		if(self.imagesAreReady && self.soundsAreReady) {
			//Add sprites to resources object
			this.resources.addSprites(this.canvas);
			//Change transition to true, to change to Menu state
			return this.transition.change = true;
		}
		return false;
	}

	/**
     * Load images reosurces
     */
	loadImages() {
		//Count for how much images have been loaded
		let loaded = 0;
		//Count number of images to load
		let numImages = this.resources.imagesSources.length;
		//Flag to tell if all images are loaded
		let ready = false;
		//Iterate trough resources.images
		for(let i = 0; i < numImages; i++) {
			//Create new Image object
			this.resources.images[i] = new Image();
			//Anonymous function to load image
			this.resources.images[i].onload = function() {
				//Increment loaded
				loaded++;
				//Increment progressBar
				self.progressBar.totalLoaded++;
				//If all images are loaded
				if(loaded >= numImages) {
					ready = true;
					self.imagesAreReady = true;					
				}
			}; 
			//Define source of image loaded
			this.resources.images[i].src = this.resources.imagesSources[i];
		}
		//Dont need to load more images
		return this.is_toLoadImages = false;
	}

	/**
     * Load sounds reosurces
     */
	loadSounds() {
		//Count for how much sounds have been loaded
		let loaded = 0;
		//Count number of sounds to load
		let numSounds = this.resources.soundsSources.length;
		//Flag to tell if all sounds are loaded
		let ready = false;
		//Iterate trough resources.sounds
		for(let i = 0; i < numSounds; i++) {
			//Create new audio object
			this.resources.sounds[i] = new Audio();
			//Event Listener with anonymous function to load audio
			this.resources.sounds[i].addEventListener('loadeddata', function() {
			    //Increment loaded
			    loaded++;
			    //Increment progressBar
			    self.progressBar.totalLoaded++;
			    //If all sounds are loaded
				if(loaded >= numSounds) {
					ready = true;
					self.soundsAreReady = true;					
				}
			}, false);
			//Define source of audio loaded
			this.resources.sounds[i].src = this.resources.soundsSources[i];
			//Remove Event Listener
			this.resources.sounds[i].removeEventListener('loadeddata', function() {}, false);
		}
		//Dont need to load mor sounds
		this.is_toLoadSounds = false;
		
	}

	/**
     * Draw Menu on canvas
     */
	draw() {
		this.clearCanvas();
		this.drawLogoImage();
		this.drawProgressBar();
		this.drawLoadingText();
	}

	/**
     * Clear our canvas for the next frame
     */
	clearCanvas() {
		this.context.clearRect(0, 0, canvas.width, canvas.height);
	}
	
	/**
     * Draw Logo image every frame
     */
	drawLogoImage() {
		//Set global Alpha to 1 (to show)
		this.context.globalAlpha = 1;

		//Draw image on canvas
		this.context.drawImage(
			this.splashImage,
			(canvas.width / 2) - this.splashImage.width / 2,
			(canvas.height / 2) - this.splashImage.height / 2);
	}

	/**
     * Draw Progress Bar every frame
     */
	drawProgressBar() {
		//Set outside progress bar color
		this.context.fillStyle = '#444444';
		//Draw progress bar background	
		this.context.fillRect(
			(self.canvas.width / 2) - self.progressBar.width / 2, 360, 
			self.progressBar.width,
			self.progressBar.height);

		//Set inside progress bar color
		this.context.fillStyle = '#eee';
		//Draw progress bar inside
		this.context.fillRect(
			(this.canvas.width / 2) - self.progressBar.width / 2, 360,
		    self.progressBar.totalLoaded * self.progressBar.width / self.progressBar.totalToLoad,
			self.progressBar.height);
	}

	/**
     * Draw Loading Text every frame
     */
	drawLoadingText() {
		//Draw Text Loading...
		this.context.fillStyle = "#fff";
		this.context.font = "12px Arial";
		this.context.textAlign = "center"; 
		this.context.textBaseline = "middle";
		this.context.fillText(
			"LOADING...",
		 	this.canvas.width / 2,
		 	380);
	}

	/**
     * Listen to keyboard input  
     *
     * @param {object} e - object that provides Event properties.
     */
	keyboardInput(e) {

	}

	/**
     * Listen to mouse input 
     *
     * @param {object} e - object that provides Event properties.
     */
	mouseInput(e) {
		
	}

	/**
     * Listen to touch input 
     * 
     * @param {object} e - object that provides Event properties
     */
    touchInput(e) {
        
    }

	/**
     * Set this state (splashscreen) active to true or false
     *
     * @param {boolean} active - A flag to know if its true or false.
     */
	setActive(active) {
		return this.active = active;
	}

	/**
     * delay before starts loading (just for presentation purpose)
     *
     * @param {number} miliseconds - Miliseconds to delay.
     */
	sleep(milisecondss) {
     	return new Promise(resolve => setTimeout(resolve, milisecondss));
    }
}