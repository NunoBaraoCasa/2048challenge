/**
 * @ Sprite file
 * @ author Nuno Barão <nunoandrebarao@outlook.pt>
 * @ version 1.0
 */

'use strict';

module.exports = class Sprite{

	/**
     * The Sprite constructor
     *
     * @param {object} image - Object that contains properties of image 
     * @param {number} width - Width of sprite
     * @param {number} height - Height of sprite
     * @param {array} positions - An array that contains what to draw from spritesheet [[x, y]]
     * @param {number} x - position on canvas
     * @param {number} y - position on canvas
     */
	constructor(image, width, height, positions, x, y) {
		this.image = image;
		this.width = width;
		this.height = height;
		this.x = x;
		this.y = y;
		this.positions = positions;
	}

	/**
     * Draw Sprite on canvas
     *
     * @param {number} position - Number 0 of index position i.e: [ 0: [x,y] ]
     * @param {object} context - object that provides methods and properties for drawing on canvas.
     */
	draw(position, context) {
		let pos = this.positions[position];
		context.drawImage(
			this.image,
			pos[0],
			pos[1],
			this.width,
			this.height,
			this.x, this.y,
			this.width,
			this.height
		);
	}

	/**
     * Set position of sprite
     *
     * @param {number} x - position on canvas
     * @param {number} y - position on canvas
     */
	setPosition(x, y) {
		this.x = x;
		this.y = y;
	}
}
