const canvas = require('canvas-prebuilt');
const CANVAS = new canvas(400, 600);
const CONTEXT = CANVAS.getContext('2d');

const jsdom = require("jsdom");
const { JSDOM } = jsdom;

const chai = require('chai');
const expect = require('chai').expect;
const Resources = require('../build/js/Resources');

chai.should();

describe('Resources', () => {

	let resources;

	beforeEach(() => {
		resources = new Resources();
	});

	describe('constructor', () => {

		it('returns an Array', () => {
			let result = resources.imagesSources;
			expect(result).to.be.a('Array');
		});

		it('returns an Array', () => {
			let result = resources.soundsSources;
			expect(result).to.be.a('Array');
		});

		it('returns an Array', () => {
			let result = resources.imagesSources;
			expect(result).to.be.a('Array');
		});

		it('returns an Array', () => {
			let result = resources.sounds;
			expect(result).to.be.a('Array');
		});

		it('returns an Object', () => {
			let result = resources.sprites;
			expect(result).to.be.a('Object');
		});

	});

	describe('newImage', () => {
		it('returns an Object', () => {
			let source = 'source';
			let result = resources.newImage(source);
			expect(result).to.be.a('Number');
		});
	});

	describe('newsound', () => {
		it('return a boolean', () => {
			let source = 'source';
			let result = resources.newSound(source);
			expect(result).to.be.a('Number');
		});
	});

	describe('addSprites', () => {
		it('return a false', () => {
			let source = 'source';
			let result = resources.addSprites(CANVAS);
			result.should.equal(false);
		});
	});
});
