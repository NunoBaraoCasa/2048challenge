const canvas = require('canvas-prebuilt');
const CANVAS = new canvas(400, 600);
const CONTEXT = CANVAS.getContext('2d');

const jsdom = require("jsdom");
const { JSDOM } = jsdom;

const chai = require('chai');
const expect = require('chai').expect;
const Sprite = require('../build/js/Sprite');

chai.should();

describe('Sprite', () => {

	let sprite;

	let image = new Image();
	let width = 0;
	let height = 0;
	let positions = [[0, 0]];
	let x = 0;
	let y = 0;

	beforeEach(() => {
		sprite = new Sprite(image, width, height, positions, x, y);
	});

	describe('constructor', () => {

		it('width returns a Number', () => {
			expect(sprite.width).to.be.a('Number');
		});

		it('height returns a Number', () => {
			expect(sprite.height).to.be.a('Number');
		});

		it('positions returns an Array', () => {
			expect(sprite.positions).to.be.a('Array');
		});

		it('x returns a Number', () => {
			expect(sprite.x).to.be.a('Number');
		});

		it('y returns a Number', () => {
			expect(sprite.y).to.be.a('Number');
		});

	});

});
