const canvas = require('canvas-prebuilt');
const CANVAS = new canvas(400, 600);
const CONTEXT = CANVAS.getContext('2d');

const jsdom = require("jsdom");
const { JSDOM } = jsdom;

const chai = require('chai');
const expect = require('chai').expect;

const dom = new JSDOM(``);

/**
 * Method to animate and make gameloop 
 */
dom.window.requestAnimFrame = (function() {
  	return dom.window.requestAnimationFrame ||
    dom.window.webkitRequestAnimationFrame ||
    dom.window.mozRequestAnimationFrame ||
    dom.window.oRequestAnimationFrame ||
    dom.window.msRequestAnimationFrame ||
    function(callback) {
      dom.window.setTimeout(callback, 1000 / 60);
    };
})();

// Tell chai that we'll be using the "should" style assertions.
chai.should();

const GameLoop = require('../build/js/GameLoop');

describe('GameLoop', () => {

	let gameLoop;

	beforeEach(() => {
		//Create a new GameLoop object before every test
		gameLoop = new GameLoop(CANVAS, CONTEXT, dom.window.requestAnimFrame);
	});

	describe('constructor', () => {

		it('returns isRunning flag', () => {
			gameLoop.isRunning.should.equal(true);
		});

		it('returns timePerFrame', () => {
			gameLoop.timePerFrame.should.equal(1/30);
		});

		it('returns timeSinceLastUpdate', () => {
			gameLoop.timeSinceLastUpdate.should.equal(0);
		});

		it('returns requestAnimFrame', () => {
			gameLoop.requestAnimFrame.should.equal(dom.window.requestAnimFrame);
		});
	});

	describe('addResources', function(){

		it('returns addResources', () => {
			let result = gameLoop.addResources();

			result.should.equal(false);
		});
	});

	describe('addStates', function(){
		it('returns addStates', () => {
			let result = gameLoop.addStates(CANVAS, CONTEXT);

			result.should.equal(false);
		});
	});
});
