const canvas = require('canvas-prebuilt');
const CANVAS = new canvas(400, 600);
const CONTEXT = CANVAS.getContext('2d');

const jsdom = require("jsdom");
const { JSDOM } = jsdom;

const chai = require('chai');
const expect = require('chai').expect;
const SplashScreen = require('../build/js/SplashScreen');

chai.should();

describe('SplashScreen', () => {

	let splashScreen;

	let active = false;
	let transition = { change: false, from: 'SplashScreen', to: 'Loading' };

	beforeEach(() => {
		splashScreen = new SplashScreen(CANVAS, CONTEXT, active, transition);
	});

	describe('constructor', () => {

		it('active returns a boolean', () => {
			splashScreen.active.should.equal(false);
		});

		it('transition returns an object', () => {
			splashScreen.transition.should.equal(transition);
		});

		it('alpha returns a Number', () => {
			let result = splashScreen.alpha;
			expect(result).to.be.a('Number');
		});

		it('is_toFadeIn returns a true', () => {
			splashScreen.is_toFadeIn.should.equal(true);
		});

		it('is_toFadeOut returns a false', () => {
			splashScreen.is_toFadeOut.should.equal(false);
		});

		it('splashScreen.src returns a String', () => {
			let result = splashScreen.splashImage.src;
			expect(result).to.be.a('String');
		});

	});

	describe('Update', () => {
		it('returns a boolean', () => {
			let deltaTime = 0.16;
			let result = splashScreen.update(deltaTime);

			expect(result).to.be.a('boolean');
		});
	});

	describe('fadeOut', () => {
		it('return a false', () => {
			let deltaTime = 0.16;
			let result = splashScreen.fadeOut(deltaTime);
			expect(result).to.be.a('boolean');
		});
	});

	describe('setActive', () => {
		it('return a boolean', () => {
			let result = splashScreen.setActive(false);

			expect(result).to.be.a('boolean');
		});
	});
});
