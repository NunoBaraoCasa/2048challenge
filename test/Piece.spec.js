const canvas = require('canvas-prebuilt');
const CANVAS = new canvas(400, 600);
const CONTEXT = CANVAS.getContext('2d');

const jsdom = require("jsdom");
const { JSDOM } = jsdom;

const chai = require('chai');
const expect = require('chai').expect;
const Piece = require('../build/js/Piece');

chai.should();

describe('Piece', () => {

	let piece;

	let x = 0;
	let y = 0;
	let value = 0;
	let color = '#000';
	let oldX = 0;
	let oldY = 0;

	beforeEach(() => {
		piece = new Piece(x, y, value, color, oldX, oldY);
	});

	describe('constructor', () => {

		it('pos returns a object vec2', () => {
			expect(piece.pos).to.be.a('object');
		});

		it('value returns a Number', () => {
			expect(piece.value).to.be.a('Number');
		});

		it('color returns an string', () => {
			expect(piece.color).to.be.a('String');
		});

		it('oldPos returns a object vec2', () => {
			expect(piece.oldPos).to.be.a('object');
		});

		it('animateFrom returns a object vec2', () => {
			expect(piece.animateFrom).to.be.a('object');
		});

		it('animateTo returns a object vec2', () => {
			expect(piece.animateTo).to.be.a('object');
		});

		it('width returns a Number', () => {
			expect(piece.width).to.be.a('Number');
		});

		it('height returns a Number', () => {
			expect(piece.height).to.be.a('Number');
		});

		it('leftOffset returns a Number', () => {
			expect(piece.leftOffset).to.be.a('Number');
		});

		it('topOffset returns a Number', () => {
			expect(piece.topOffset).to.be.a('Number');
		});

		it('inbetweenOffset returns a Number', () => {
			expect(piece.inbetweenOffset).to.be.a('Number');
		});

		it('speed returns a Number', () => {
			expect(piece.speed).to.be.a('Number');
		});

		it('direction returns a Number', () => {
			expect(piece.direction).to.be.a('Number');
		});

		it('colors returns a object', () => {
			expect(piece.colors).to.be.a('Object');
		});

	});

	describe('Update Color', () => {
		it('returns a boolean', () => {
			let piece = new Piece();
			let result = piece.updateColor(piece);

			expect(result).to.be.a('Object');
		});
	});

});
