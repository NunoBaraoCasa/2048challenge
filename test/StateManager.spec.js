const canvas = require('canvas-prebuilt');
const CANVAS = new canvas(400, 600);
const CONTEXT = CANVAS.getContext('2d');

const jsdom = require("jsdom");
const { JSDOM } = jsdom;

const chai = require('chai');
const expect = require('chai').expect;
const StateManager = require('../build/js/StateManager');

chai.should();

describe('StateManager', () => {

	let stateManager;

	beforeEach(() => {
		stateManager = new StateManager(CONTEXT);
	});

	describe('constructor', () => {

		it('stack returns an Array', () => {
			expect(stateManager.stack).to.be.a('Array');
		});

		it('states returns an Array', () => {
			expect(stateManager.states).to.be.a('Array');
		});

	});

});
