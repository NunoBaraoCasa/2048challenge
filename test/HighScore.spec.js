const canvas = require('canvas-prebuilt');
const CANVAS = new canvas(400, 600);
const CONTEXT = CANVAS.getContext('2d');

const jsdom = require("jsdom");
const { JSDOM } = jsdom;

const chai = require('chai');
const expect = require('chai').expect;
const HighScore = require('../build/js/HighScore');

chai.should();

describe('HighScore', () => {

	let highScore;

	let resources = {
		imagesSources:
			[ 	'images/splash_logo.png',
	    		'images/spritesheet.png',
	    		'images/square.png' 
	    	],
	  soundsSources: [],
	  images: [],
	  sounds: [],
	  sprites: {} 
	};	
	let active = false;
	let transition = { change: false, from: 'HighScore', to: 'Menu' };

	beforeEach(() => {
		highScore = new HighScore(CANVAS, CONTEXT, resources, active, transition);
	});

	describe('constructor', () => {

		it('active returns boolean', () => {
			highScore.active.should.equal(false);
		});

		it('resources returns object', () => {
			highScore.resources.should.equal(resources);
		});

		it('transition returns object', () => {
			highScore.transition.should.equal(transition);
		});

	});

	describe('Update', () => {
		it('returns a boolean', () => {
			let deltaTime = 0.16;
			let result = highScore.update(deltaTime);

			expect(result).to.be.a('boolean');
		});
	});

	describe('setActive', () => {
		it('return a boolean', () => {
			let result = highScore.setActive(false);

			expect(result).to.be.a('boolean');
		});
	});


});
