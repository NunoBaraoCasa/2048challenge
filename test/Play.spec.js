const canvas = require('canvas-prebuilt');
const CANVAS = new canvas(400, 600);
const CONTEXT = CANVAS.getContext('2d');

const jsdom = require("jsdom");
const { JSDOM } = jsdom;

const chai = require('chai');
const expect = require('chai').expect;
const Play = require('../build/js/Play');
const Piece = require('../build/js/Piece');

chai.should();

describe('Play', () => {

	let play;

	let resources = {
		imagesSources:
			[ 	'images/splash_logo.png',
	    		'images/spritesheet.png',
	    		'images/square.png' 
	    	],
	  soundsSources: [],
	  images: [],
	  sounds: [],
	  sprites: {} 
	};	
	let active = false;
	let transition = { change: false, from: 'Play', to: 'Menu' };

	beforeEach(() => {
		play = new Play(CANVAS, CONTEXT, resources, active, transition);
	});

	describe('constructor', () => {

		it('active returns a boolean', () => {
			play.active.should.equal(false);
		});

		it('resources returns an array', () => {
			play.resources.should.equal(resources);
		});

		it('transition returns an object', () => {
			play.transition.should.equal(transition);
		});

		it('gridSize returns an Number', () => {
			expect(play.gridSize).to.be.a('Number');
		});

		it('slot returns a object ', () => {
			expect(play.slot).to.be.a('object');
		});

		it('direction returns a Object', () => {
			expect(play.direction).to.be.a('Object');
		});

		it('orientation returns an Object', () => {
			expect(play.orientation).to.be.a('Object');
		});

		it('touch returns a object ', () => {
			expect(play.touch).to.be.a('object');
		});

		it('swipe returns a object ', () => {
			expect(play.swipe).to.be.a('object');
		});

		it('grid returns a Array ', () => {
			expect(play.grid).to.be.a('Array');
		});

		it('emptyPieces returns a Array', () => {
			expect(play.emptyPieces).to.be.a('Array');
		});

		it('keyState returns a Object', () => {
			expect(play.keyState).to.be.a('Object');
		});

		it('keyboard returns a Object', () => {
			expect(play.keyboard).to.be.a('Object');
		});

		it('is_toMove returns a Boolean', () => {
			expect(play.is_toMove).to.be.a('Boolean');
		});

		it('is_toUpdateEmpty returns a Boolean', () => {
			expect(play.is_toUpdateEmpty).to.be.a('Boolean');
		});

		it('score returns a Number', () => {
			expect(play.score).to.be.a('Number');
		});

		it('is_gameOver returns a Boolean', () => {
			expect(play.is_gameOver).to.be.a('Boolean');
		});

		it('is_toChange returns a Boolean', () => {
			expect(play.is_toChange).to.be.a('Boolean');
		});

		it('is_toAnimate returns a Boolean', () => {
			expect(play.is_toAnimate).to.be.a('Boolean');
		});

		it('is_toDraw returns a Boolean', () => {
			expect(play.is_toDraw).to.be.a('Boolean');
		});

		it('gridChanged returns a Image', () => {
			expect(play.gridChanged).to.be.a('Boolean');
		});

		it('logo.src  returns a String', () => {
			expect(play.logo.src ).to.be.a('String');
		});
		

	});

	describe('Setup Grid', () => {
		it('returns a false', () => {
			let result = play.setupGrid();
			result.should.equal(false);
		});
	});

	describe('Update', () => {
		it('returns a boolean', () => {
			let deltaTime = 0.16;
			let result = play.update(deltaTime);

			expect(result).to.be.a('boolean');
		});
	});

	describe('movePieces', () => {
		it('returns a false', () => {
			let direction = 1;
			let orientation = 1;
			let result = play.movePieces(direction, orientation);
			result.should.equal(false);
		});
	});

	describe('moveHorizontaly', () => {
		it('returns a false', () => {
			let direction = 1;
			let orientation = 1;
			let result = play.moveHorizontaly(direction, orientation);
			result.should.equal(false);
		});
	});

	describe('moveVertically', () => {
		it('returns a false', () => {
			let direction = 1;
			let orientation = 1;
			let result = play.moveVertically(direction, orientation);
			result.should.equal(false);
		});
	});

	describe('handleRow', () => {
		it('returns an Array', () => {
			let direction = 1;
			let orientation = 1;
			let arr = [0, 2, 0, 0];
			let result = play.handleRow(direction, arr, orientation);
			expect(result).to.be.a('Array');
		});
	});

	describe('getColumn', () => {
		it('returns an Array', () => {
			let grid = [
				[0, 0, 0, 0],
				[2, 2, 2, 0],
				[0, 2, 2, 0],
				[2, 2, 0, 2]
			];
			let col = 1;
			let result = play.getColumn(grid, col);
			expect(result).to.be.a('Array');
		});
	});

	describe('manipulateRow', () => {
		it('returns an Array', () => {
			let direction = 1;
			let arr = [0, 2, 0, 0];
			let result = play.manipulateRow(direction, arr);
			expect(result).to.be.a('Array');
		});
	});

	describe('rowWithZeros', () => {
		it('returns an Array', () => {
			let arr = [0, 2, 0, 0];
			let result = play.RowWithZeros(arr);
			expect(result).to.be.a('Array');
		});
	});

	describe('rowWithoutZeros', () => {
		it('returns an Array', () => {
			let arr = [0, 2, 0, 0];
			let result = play.rowWithoutZeros(arr);
			expect(result).to.be.a('Array');
		});
	});

	describe('flipRowByOrientation', () => {
		it('returns an Array', () => {
			let direction = 1;
			let arr = [0, 2, 0, 0];
			let result = play.flipRowByOrientation(direction, arr);
			expect(result).to.be.a('Array');
		});
	});

	describe('sum', () => {
		it('returns an Array', () => {
			let piece = new Piece();
			let row = [piece, piece, piece, piece];
			let result = play.sum(row);
			expect(result).to.be.a('Array');
		});
	});

	describe('updatePiecesPosition', () => {
		it('returns a false', () => {
			let piece = new Piece();
			let orientation = 1;
			let arr = [piece, piece, piece, piece];
			let result = play.updatePiecesPosition(arr, orientation);
			result.should.equal(false);
		});
	});

	describe('updatePiecesDirection', () => {
		it('returns a false', () => {
			let piece = new Piece();
			let orientation = 1;
			let arr = [piece, piece, piece, piece];
			let result = play.updatePiecesDirection(arr, orientation);
			result.should.equal(false);
		});
	});

	describe('mergeRows', () => {
		it('returns an Array', () => {
			let direction = 1;
			let piece = new Piece();
			let zerosRow = [piece, piece, piece, piece];
			let rowToSum = [piece, piece, piece, piece];
			let result = play.mergeRows(direction, rowToSum, zerosRow);
			expect(result).to.be.a('Array');
		});
	});

	describe('checkChanges', () => {
		it('returns a false', () => {
			let result = play.checkChanges();
			result.should.equal(false);
		});
	});

	describe('checkIfPieceMoved', () => {
		it('returns a boolean', () => {
			let piece = new Piece();
			let result = play.checkIfPieceMoved(piece);
			expect(result).to.be.a('Boolean');
		});
	});

	describe('checkWinOrLose', () => {
		it('returns a boolean', () => {
			let result = play.checkWinOrLose();
			expect(result).to.be.a('Boolean');
		});
	});

	describe('checkIfPlayerWins', () => {
		it('returns a boolean', () => {
			let piece = new Piece();
			let result = play.checkIfPlayerWins(piece);
			expect(result).to.be.a('Boolean');
		});
	});

	describe('checkEmptyPiece', () => {
		it('returns a boolean', () => {
			let piece = new Piece();
			let result = play.checkEmptyPiece(piece);
			expect(result).to.be.a('Boolean');
		});
	});

	describe('checkVerticalMoves', () => {
		it('returns a boolean', () => {
			let piece = new Piece();
			let x = 0;
			let y = 0;
			let result = play.checkVerticalMoves(piece, piece, x, y);
			expect(result).to.be.a('Boolean');
		});
	});

	describe('checkHorizontalMoves', () => {
		it('returns a boolean', () => {
			let piece = new Piece();
			let x = 0;
			let y = 0;
			let result = play.checkHorizontalMoves(piece, piece, x, y);
			expect(result).to.be.a('Boolean');
		});
	});

	describe('addPiece', () => {
		it('returns a false', () => {
			let result = play.addPiece();
			result.should.equal(false);
		});
	});

	describe('randomNumber', () => {
		it('returns a Number', () => {
			let min = 0;
			let max = 1;
			let result = play.randomNumber(min, max);
			expect(result).to.be.a('Number');
		});
	});

	describe('resetEverything', () => {
		it('returns a false', () => {
			let result = play.resetEverything();
			result.should.equal(false);
		});
	});

	describe('setActive', () => {
		it('return a boolean', () => {
			let result = play.setActive(false);

			expect(result).to.be.a('boolean');
		});
	});

});
