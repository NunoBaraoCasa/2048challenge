const canvas = require('canvas-prebuilt');
const CANVAS = new canvas(400, 600);
const CONTEXT = CANVAS.getContext('2d');

const jsdom = require("jsdom");
const { JSDOM } = jsdom;

const chai = require('chai');
const expect = require('chai').expect;
const Loading = require('../build/js/Loading');

chai.should();

describe('Loading', () => {

	let loading;

	let resources = {
		imagesSources:
			[ 	'images/splash_logo.png',
	    		'images/spritesheet.png',
	    		'images/square.png' 
	    	],
	  soundsSources: [],
	  images: [],
	  sounds: [],
	  sprites: {} 
	};	
	let active = false;
	let transition = { change: false, from: 'Loading', to: 'Menu' };

	beforeEach(() => {
		loading = new Loading(CANVAS, CONTEXT, resources, active, transition);
	});

	describe('constructor', () => {

		it('active returns a boolean', () => {
			loading.active.should.equal(false);
		});

		it('resources returns an array', () => {
			loading.resources.should.equal(resources);
		});

		it('transition returns an object', () => {
			loading.transition.should.equal(transition);
		});

		it('is_toLoadImages returns a boolean', () => {
			loading.is_toLoadImages.should.equal(true);
		});

		it('is_toLoadSounds returns a boolean', () => {
			loading.is_toLoadSounds.should.equal(false);
		});

		it('splashImage Src returns a string', () => {
			let result = loading.splashImage.src;

			expect(result).to.be.a('string');
		});
		
		it('is_toDelay returns a boolean', () => {
			loading.is_toDelay.should.equal(true);
		});

	});

	describe('Update', () => {
		it('returns a boolean', () => {
			let deltaTime = 0.16;
			let result = loading.update(deltaTime);

			expect(result).to.be.a('boolean');
		});
	});

	describe('setActive', () => {
		it('return a boolean', () => {
			let result = loading.setActive(false);

			expect(result).to.be.a('boolean');
		});
	});

	describe('resourcesReady', () => {
		it('return a boolean', () => {
			let result = loading.resourcesReady();

			expect(result).to.be.a('boolean');
		});
	});

});
