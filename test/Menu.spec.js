const canvas = require('canvas-prebuilt');
const CANVAS = new canvas(400, 600);
const CONTEXT = CANVAS.getContext('2d');

const jsdom = require("jsdom");
const { JSDOM } = jsdom;

const chai = require('chai');
const expect = require('chai').expect;
const Menu = require('../build/js/Menu');

chai.should();

describe('Menu', () => {

	let menu;

	let resources = {
		imagesSources:
			[ 	'images/splash_logo.png',
	    		'images/spritesheet.png',
	    		'images/square.png' 
	    	],
	  soundsSources: [],
	  images: [],
	  sounds: [],
	  sprites: {} 
	};	
	let active = false;
	let transition = { change: false, from: 'Menu', to: 'Play' };

	beforeEach(() => {
		menu = new Menu(CANVAS, CONTEXT, resources, active, transition);
	});

	describe('constructor', () => {

		it('active returns a boolean', () => {
			menu.active.should.equal(false);
		});

		it('resources returns an array', () => {
			menu.resources.should.equal(resources);
		});

		it('transition returns an object', () => {
			menu.transition.should.equal(transition);
		});

	});

	describe('Update', () => {
		it('returns a boolean', () => {
			let deltaTime = 0.16;
			let result = menu.update(deltaTime);

			expect(result).to.be.a('boolean');
		});
	});

	describe('setActive', () => {
		it('return a boolean', () => {
			let result = menu.setActive(false);

			expect(result).to.be.a('boolean');
		});
	});

});
